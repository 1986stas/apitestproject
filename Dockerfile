FROM python:3.7
MAINTAINER Stas Osikov

RUN apt-get update && apt-get install \
  -y --no-install-recommends python3 python3-virtualenv

RUN mkdir -p /app/
WORKDIR /app/
COPY . /app/

RUN pip install -r requirements.txt
RUN python superservice.py migrate
RUN python superservice.py

EXPOSE 5000

COPY superservice.py .

CMD ["python", "superservice.py"]
ENTRYPOINT ["tail", "-f", "/dev/null"]

