package org.example.stas.osikov.api;

import com.fasterxml.jackson.databind.JsonNode;
import org.example.stas.osikov.api.annotations.TestData;
import org.example.stas.osikov.api.listeners.CustomTestNGListener;
import org.example.stas.osikov.api.steps.BaseStep;
import org.example.stas.osikov.api.steps.UserSteps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;

import java.lang.reflect.Method;
import java.net.URL;

import static org.example.stas.osikov.api.mapper.JsonMapper.readToJsonNode;

@Listeners(value = {CustomTestNGListener.class})
@ContextConfiguration(classes = BaseStep.class)
public class BaseTest extends AbstractTestNGSpringContextTests {

    protected static final String DATA_PROVIDER_METHOD = "loadTestDataFromJson";

    @Autowired
    protected UserSteps userSteps;

    @DataProvider(name = DATA_PROVIDER_METHOD)
    public Object[][] loadTestDataFromJson(Method method) {
        String jsonPath = new StringBuilder().
                append("/").
                append(method.getAnnotation(TestData.class).value()).
                toString();
        URL url = this.getClass().getResource(jsonPath);
        JsonNode jsonNode = readToJsonNode(url);
        return new Object[][]{{jsonNode}};
    }
}