package org.example.stas.osikov.api.unitTests;

import org.example.stas.osikov.api.BaseTest;
import org.example.stas.osikov.api.utils.TextUtils;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class TextUtilsTest extends BaseTest {

    @Test
    public void testNullString()  {
        assertTrue(TextUtils.isEmpty(null));
    }

    @Test
    public void testEmptyString() {
        assertTrue(TextUtils.isEmpty(""));
    }

    @Test
    public void testNotEmptyString() {
        assertFalse(TextUtils.isEmpty("not_empty_string"));
    }
}

