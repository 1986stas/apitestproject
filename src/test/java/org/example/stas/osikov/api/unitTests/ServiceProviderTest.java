package org.example.stas.osikov.api.unitTests;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import org.example.stas.osikov.api.BaseTest;
import org.example.stas.osikov.api.services.ServiceProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class ServiceProviderTest extends BaseTest {

    @Test
    public void testLoggingInterceptor() {
        Interceptor interceptor = ServiceProvider.getLoggingInterceptor();
        assertNotNull(interceptor);
    }

    @Test
    public void testTokenInterceptor() {
        Interceptor interceptor = ServiceProvider.getTokenInterceptor("token");
        assertNotNull(interceptor);
    }

    @Test
    public void testClient() {
        OkHttpClient client = ServiceProvider.client();
        assertNotNull(client);
    }
}
