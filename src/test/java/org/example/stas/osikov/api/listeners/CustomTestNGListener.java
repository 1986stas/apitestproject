package org.example.stas.osikov.api.listeners;

import com.fasterxml.jackson.databind.JsonNode;
import org.example.stas.osikov.api.logger.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;


public class CustomTestNGListener extends TestListenerAdapter implements ITestListener {

    @Override
    public void onTestStart(ITestResult result) {
        Logger.LOGGER.info("Test Started: {}::{}", result.getMethod().getTestClass().getName(),
                result.getMethod().getMethodName());
        logTestData(result);
    }

    @Override
    public void onTestSuccess(ITestResult result) {
    }

    @Override
    public void onTestFailure(ITestResult result) {
        Logger.LOGGER.info("Test Failed: {}::{}", result.getMethod().getTestClass().getName(),
                result.getMethod().getMethodName());
        logTestData(result);
    }

    @Override
    public void onTestSkipped(ITestResult result) {
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
    }

    @Override
    public void onStart(ITestContext context) {
    }

    @Override
    public void onFinish(ITestContext context) {
    }

    private void logTestData(ITestResult testResult) {
        if (testResult.getParameters().length != 0) {
            JsonNode jsonNode = (JsonNode) testResult.getParameters()[0];
            Logger.LOGGER.info("Test data:\n {}", jsonNode.toString());
        }
    }
}