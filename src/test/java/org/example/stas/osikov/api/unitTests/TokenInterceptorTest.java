package org.example.stas.osikov.api.unitTests;

import okhttp3.Interceptor;
import org.example.stas.osikov.api.BaseTest;
import org.example.stas.osikov.api.interceptor.TokenInterceptor;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class TokenInterceptorTest extends BaseTest {

    private static final String TEST_TOKEN = "test_token";

    @Test
    public void testConstructor() {
        Interceptor interceptor = TokenInterceptor.getInstance(TEST_TOKEN);
        assertNotNull(interceptor);
    }
}

