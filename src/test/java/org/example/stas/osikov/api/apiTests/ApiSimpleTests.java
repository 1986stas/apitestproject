package org.example.stas.osikov.api.apiTests;

import com.fasterxml.jackson.databind.JsonNode;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.assertj.core.api.Assertions;
import org.example.stas.osikov.api.BaseTest;
import org.example.stas.osikov.api.annotations.TestData;
import org.example.stas.osikov.api.dto.PayloadDTO;
import org.example.stas.osikov.api.dto.PayloadResponseDTO;
import org.example.stas.osikov.api.dto.UserDTO;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@Feature(value = "In this feature we're going to check possibility to create new user, get status code and create payload")
public class ApiSimpleTests extends BaseTest {

    private UserDTO userDTO;
    private final static String PREFIX = "Bearer ";

    @Test(dataProvider = DATA_PROVIDER_METHOD)
    @Description(value = "Test token not null")
    @TestData("userTestData/testUserData.json")
    public void testTokenNotNull(final JsonNode jsonNode) {
        String userName = jsonNode.get("user_name").asText();
        String userPassword = jsonNode.get("user_password").asText();
        userDTO = new UserDTO(userName, userPassword);
        assertThat(userSteps.getAuthorizationToken(userDTO), is(notNullValue()));
    }

    @Test(dataProvider = DATA_PROVIDER_METHOD)
    @Description(value = "Test token not null")
    @TestData("userTestData/testUserData.json")
    public void testTokenNoTheSame(final JsonNode jsonNode) {
        String userName = jsonNode.get("user_name").asText();
        String userPassword = jsonNode.get("user_password").asText();
        userDTO = new UserDTO(userName, userPassword);
        final String firstGeneratedToken = userSteps.getAuthorizationToken(userDTO).getToken();
        final String secondGeneratedToken = userSteps.getAuthorizationToken(userDTO).getToken();
        assertThat(firstGeneratedToken, is(not(equalTo(secondGeneratedToken))));
    }

    @Test(dataProvider = DATA_PROVIDER_METHOD)
    @Description(value = "Test status code ok")
    @TestData("userTestData/testUserData.json")
    public void testStatusCode(final JsonNode jsonNode) {
        final int statusCodeOk = jsonNode.get("status_code").asInt();
        Assertions.assertThat(userSteps.getStatusCode()).as("Response code").isEqualTo(statusCodeOk);
    }

    @Test(dataProvider = DATA_PROVIDER_METHOD)
    @Description("In this test we are going to check that integer value is not allowed")
    @TestData("userTestData/testUserData.json")
    public void testUnsuccessCreatePayload(final JsonNode jsonNode){
        String userName = jsonNode.get("user_name").asText();
        String userPassword = jsonNode.get("user_password").asText();
        int request = jsonNode.get("badRequestInteger").asInt();
        PayloadDTO payloadDTO = new PayloadDTO(request);
        userDTO = new UserDTO(userName, userPassword);
        final String token = userSteps.getAuthorizationToken(userDTO).getToken();
        userSteps.getBadPayloadResponse(PREFIX + token, payloadDTO);
    }

    @Test(dataProvider = DATA_PROVIDER_METHOD)
    @Description("In this test we are going to check that integer value is not allowed")
    @TestData("userTestData/testUserData.json")
    public void testSuccessCreatePayload(final JsonNode jsonNode){
        String userName = jsonNode.get("user_name").asText();
        String userPassword = jsonNode.get("user_password").asText();
        String request = jsonNode.get("goodRequestString").asText();
        String errorMessage = jsonNode.get("errorMessage").asText();
        PayloadDTO payloadDTO = new PayloadDTO(request);
        userDTO = new UserDTO(userName, userPassword);
        final String token = userSteps.getAuthorizationToken(userDTO).getToken();
        PayloadResponseDTO payloadResponseDTO = userSteps.getGoodPayloadResponse(PREFIX + token, payloadDTO);
        if (payloadResponseDTO.getStatus().contains("ERROR")){
            assertThat("Wrong error message", payloadResponseDTO.getError().equals(errorMessage));
        }
        else assertThat("Success message is wrong", payloadResponseDTO.getStatus().equals("OK"));
    }
}

