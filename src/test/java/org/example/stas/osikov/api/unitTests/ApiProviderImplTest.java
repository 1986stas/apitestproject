package org.example.stas.osikov.api.unitTests;

import org.example.stas.osikov.api.BaseTest;
import org.example.stas.osikov.api.services.ApiProviderImpl;
import org.example.stas.osikov.api.services.IApiProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class ApiProviderImplTest extends BaseTest {

    @Test
    public void testConstructor() {
        IApiProvider provider = new ApiProviderImpl("test");
        assertNotNull(provider);
    }

    @Test
    public void testToken() {
        IApiProvider provider = new ApiProviderImpl("test");
        assertEquals("test", provider.provideToken());
    }

    @Test
    public void testGithubService() {
        IApiProvider provider = new ApiProviderImpl("test");
        assertNotNull(provider.provideUserService());
    }
}