package org.example.stas.osikov.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import lombok.Getter;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "status",
        "error"
})
public class PayloadResponseDTO {
    @Getter
    @JsonProperty("id")
    private Integer id;
    @Getter
    @JsonProperty("status")
    private String status;
    @Getter
    @JsonProperty("error")
    private String error;
}
