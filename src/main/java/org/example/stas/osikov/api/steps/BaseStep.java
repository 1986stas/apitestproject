package org.example.stas.osikov.api.steps;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "org.example.stas.osikov.api.steps")
public class BaseStep {
}
