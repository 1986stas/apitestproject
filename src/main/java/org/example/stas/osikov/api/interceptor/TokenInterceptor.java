package org.example.stas.osikov.api.interceptor;

import lombok.NonNull;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import org.example.stas.osikov.api.utils.TextUtils;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

/**
 * this class can be used for future improvements
 */
public final class TokenInterceptor implements Interceptor {

    private final String mToken;

    private TokenInterceptor(@NonNull String token) {
        mToken = token;
    }

    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        if (TextUtils.isEmpty(mToken)) {
            return chain.proceed(chain.request());
        }
        Request request = chain.request().newBuilder()
                .addHeader("Authorization", String.format("%s %s", "token", mToken))
                .build();
        return chain.proceed(request);
    }

    @NonNull
    public static Interceptor getInstance(@NonNull String token) {
        return new TokenInterceptor(token);
    }
}
