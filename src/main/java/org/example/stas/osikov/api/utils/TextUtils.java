package org.example.stas.osikov.api.utils;

import org.jetbrains.annotations.Nullable;

public final class TextUtils {

    private TextUtils() {
    }

    public static boolean isEmpty(@Nullable CharSequence text) {
        return text == null || text.length() == 0;
    }
}
