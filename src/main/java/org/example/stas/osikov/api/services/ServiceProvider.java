package org.example.stas.osikov.api.services;

import lombok.NonNull;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.example.stas.osikov.api.interceptor.TokenInterceptor;

import java.util.concurrent.TimeUnit;

public class ServiceProvider {

    private static OkHttpClient sClient;

    private static final int CONNECT_TIMEOUT = 15;
    private static final int READ_TIMEOUT = 30;
    private static final int WRITE_TIMEOUT = 30;

    private ServiceProvider(){
    }

    @NonNull
    public static OkHttpClient client() {
        OkHttpClient client = sClient;
        if (client == null) {
            synchronized (ServiceProvider.class) {
                client = sClient;
                if (client == null) {
                    client = sClient = getOkHttpClient();
                }
            }
        }
        return client;
    }

    @NonNull
    public static OkHttpClient getOkHttpClient() {
        sClient = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(getLoggingInterceptor())
                .build();
        return sClient;
    }

    @NonNull
    public static Interceptor getLoggingInterceptor() {
        return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    @NonNull
    public static Interceptor getTokenInterceptor(@NonNull String token) {
        return TokenInterceptor.getInstance(token);
    }
}

