package org.example.stas.osikov.api.services;

import lombok.NonNull;
import org.example.stas.osikov.api.services.response.UserResponseClient;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import static org.example.stas.osikov.api.config.Properties.CONFIG;

public class ApiProviderImpl implements IApiProvider {
    private final String mToken;
    private final UserResponseClient responseClient;

    public ApiProviderImpl(@NonNull String token){
        mToken = token;
        responseClient = createUserService();
    }

    public static UserResponseClient createUserService(){
        return new Retrofit.Builder()
                .baseUrl(CONFIG.host())
                .client(ServiceProvider.client())
                .addConverterFactory(JacksonConverterFactory.create())
                .build()
                .create(UserResponseClient.class);
    }

    @Override
    public @NonNull UserResponseClient provideUserService() {
        return responseClient;
    }

    @Override
    public @NonNull String provideToken() {
        return mToken;
    }
}
