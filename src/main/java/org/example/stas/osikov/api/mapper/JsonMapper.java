package org.example.stas.osikov.api.mapper;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.example.stas.osikov.api.exceptions.TestDataException;
import org.example.stas.osikov.api.logger.Logger;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

public class JsonMapper<T> {

    private static final ObjectMapper mapper = new ObjectMapper();

    public static JsonNode readToJsonNode(final URL url) {
        try {
            return mapper.readTree(url);
        } catch (IOException e) {
            throw new TestDataException("Failed to read url to JsonNode: %s", url, e);
        }
    }

    public static <T> T readToObject(final JsonNode jsonNode, final Class<T> valueType) {
        String jsonNodeContent = jsonNode.toString();
        try {
            return mapper.readValue(jsonNodeContent, valueType);
        } catch (IOException e) {
            throw new TestDataException("Failed to read JsonNode content to %s object: %s", valueType.getName(),
                    jsonNodeContent, e);
        }
    }

    public List<T> getDataAsList(String nodeName) {
        try {
            ObjectNode node = (ObjectNode) mapper.readTree(new File("./src/test/resources/testUserData/testUserData.json"));
            JsonNode jsonNode = node.get(nodeName);
            return mapper.readValue(jsonNode.traverse(), new TypeReference<List<T>>() {
            });

        } catch (IOException e) {
            Logger.LOGGER.error("Error occurs during parse json \n" + e.getMessage());
            return null;
        }
    }
}