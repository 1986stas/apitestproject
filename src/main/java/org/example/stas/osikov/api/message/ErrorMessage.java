package org.example.stas.osikov.api.message;

public class ErrorMessage {

    private ErrorMessage() {
    }

    public static String forNoMatch(final String element) {
        return String.format("'%s' do not match", element);
    }
}