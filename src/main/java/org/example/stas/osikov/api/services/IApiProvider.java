package org.example.stas.osikov.api.services;

import lombok.NonNull;
import org.example.stas.osikov.api.services.response.UserResponseClient;

public interface IApiProvider {

    @NonNull
    UserResponseClient provideUserService();
    @NonNull
    String provideToken();
}
