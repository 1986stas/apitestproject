package org.example.stas.osikov.api.exceptions;

public class TestDataException extends RuntimeException {

    public TestDataException(final String messageTemplate, Object... args) {
        super(String.format(messageTemplate, args));
    }
}