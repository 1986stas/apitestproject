package org.example.stas.osikov.api.services.response;

import okhttp3.ResponseBody;
import org.example.stas.osikov.api.dto.AuthorizationDTO;
import org.example.stas.osikov.api.dto.PayloadDTO;
import org.example.stas.osikov.api.dto.PayloadResponseDTO;
import retrofit2.Call;
import retrofit2.http.*;

public interface UserResponseClient {

    @Headers("Content-Type: application/json")
    @POST("/api/save_data/")
    Call<PayloadResponseDTO> createPayload(@Header("Authorization") String token, @Body PayloadDTO payloadDTO);

    @FormUrlEncoded
    @POST("/authorize/")
    Call<AuthorizationDTO> getToken(@Field("username") String name, @Field("password") String password);

    @GET("/ping/")
    Call<ResponseBody> getStatusCode();
}
