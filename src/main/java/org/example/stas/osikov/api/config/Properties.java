package org.example.stas.osikov.api.config;

import org.aeonbits.owner.ConfigFactory;

public class Properties {

    public static final Configuration CONFIG = ConfigFactory.create(Configuration.class, System.getProperties());

    private Properties() {
    }
}