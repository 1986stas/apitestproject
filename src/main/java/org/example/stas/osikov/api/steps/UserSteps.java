package org.example.stas.osikov.api.steps;

import io.qameta.allure.Step;
import lombok.NonNull;
import org.apache.commons.httpclient.HttpStatus;
import org.example.stas.osikov.api.dto.AuthorizationDTO;
import org.example.stas.osikov.api.dto.PayloadDTO;
import org.example.stas.osikov.api.dto.PayloadResponseDTO;
import org.example.stas.osikov.api.dto.UserDTO;
import org.example.stas.osikov.api.services.ApiProviderImpl;
import org.springframework.stereotype.Component;
import retrofit2.Response;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@Component
public class UserSteps {

    @Step
    public AuthorizationDTO getAuthorizationToken(@NonNull UserDTO userDTO) {
        try {
            Response tokenResponse = ApiProviderImpl.
                    createUserService().
                    getToken(userDTO.getName(), userDTO.getPassword()).
                    execute();
            assertThat(tokenResponse.code()).as("Response code").isEqualTo(HttpStatus.SC_OK);
            return (AuthorizationDTO) tokenResponse.body();
        } catch (IOException e) {
            throw new IllegalStateException("Failed to execute post request to token", e);
        }
    }

    @Step
    public Object getStatusCode() {
        try {
            Response statusCodeResponse = ApiProviderImpl.
                    createUserService().
                    getStatusCode().
                    execute();
            return statusCodeResponse.code();
        } catch (IOException e) {
            throw new IllegalStateException("Failed to execute get request to status code", e);
        }
    }

    @Step
    public PayloadResponseDTO getGoodPayloadResponse(@NonNull String authorizationDTO, @NonNull PayloadDTO payloadDTO){
        try {
            Response payloadResponse = ApiProviderImpl.
                    createUserService().
                    createPayload(authorizationDTO, payloadDTO).
                    execute();
            assertThat(payloadResponse.code()).as("Response code").isEqualTo(HttpStatus.SC_OK);
            return (PayloadResponseDTO) payloadResponse.body();
        } catch (IOException e) {
            throw new IllegalStateException("Failed to execute post request to payload", e);
        }
    }

    @Step
    public PayloadResponseDTO getBadPayloadResponse(@NonNull String authorizationDTO, @NonNull PayloadDTO payloadDTO){
        try {
            Response payloadResponse = ApiProviderImpl.
                    createUserService().
                    createPayload(authorizationDTO, payloadDTO).
                    execute();
            assertThat(payloadResponse.code()).as("Response code").isEqualTo(HttpStatus.SC_BAD_REQUEST);
            return (PayloadResponseDTO) payloadResponse.body();
        } catch (IOException e) {
            throw new IllegalStateException("Failed to execute post request to payload", e);
        }
    }
}
