package org.example.stas.osikov.api.dto;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "token"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthorizationDTO {
    @Getter
    @JsonProperty("token")
    private String token;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

}
