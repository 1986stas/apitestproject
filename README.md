TO run tests run through the following instruction:
###1. Install and run python service according the following steps
* 1.1. install python, python virtualenv
* 1.2. run the following commands to create new python virtual environment: virtualenv venv
* 1.3. install script dependencies venv/bin/pip install sqlalchemy bottle
* 1.4. create local database structure venv/bin/python superservice.py migrate
* 1.5. start local web service venv/bin/python superservice.py run

###2. Perform gradle clean test allureReport 